import unittest, logging, sqlite3, csv

def lecture_fichier_csv(chemin_csv, base_de_donnee):
    with open(chemin_csv, 'r', newline='') as csvfile:
        logging.info("Ouverture du fichier csv")

        lecture_csv = csv.reader(csvfile, delimiter='|')
        header = True
        for row in lecture_csv:
            if header == True:
                header=False
            else:
                Insertion_ligne_bdd(row, base_de_donnee)
    logging.info("Fermeture du fichier csv")

def connection_base_de_donnee():
    logging.info("Connection base de donnée")
    base_de_donnee = sqlite3.connect('bdd_import.sqlite3')
    return base_de_donnee

def verification_table_bdd(base_de_donnee):
    logging.info("Vérification de la table")
    requete = base_de_donnee.cursor()
    requete.execute('CREATE TABLE IF NOT EXISTS Voiture(address TEXT, carrosserie TEXT, categorie TEXT, couleur TEXT, cylindree INT, date_immat TEXT, denomination TEXT, energy TEXT, firstname TEXT, immat TEXT PRIMARY KEY, marque TEXT, name TEXT, places INT, poids INT, puissance INT, type_variante_version TEXT, vin TEXT)')
    base_de_donnee.commit()
    logging.info("Fin de vérification")

def Insertion_ligne_bdd(row, base_de_donnee):
    pass

if __name__ == '__main__':

    logging.basicConfig(filename="log_bdd.log", format='%(asctime)s %(message)s')
    connection_base_de_donnee()

    logging.info("Début du programme")
    fichier_csv = "csv/csv_auto.csv"

    verification_table_bdd(base_de_donnee)

    lecture_fichier_csv(fichier_csv, base_de_donnee)

    base_de_donnee.close()
    logging.info("Déconnection base de donnée")
    logging.info("Fin de programme")
    


import unitest, os
from programme import lecture_fichier_csv, connection_base_de_donnee, creation_bdd, verification_table_bdd

class Test(unittest.TestCase):

    def lecture_fichier_csv(self):

        self.assertEqual(True, lecture_fichier_csv("csv_de_test.csv"))
        self.assertEqual(False, lecture_fichier_csv("csv_de_test.c"))
        self.assertEqual(False, lecture_fichier_csv("csv_test_oskur.csv"))
    
    def connection_base_de_donnee(self):

        self.assertEqual(True, connection_base_de_donnee("bdd_import.sqlite3"))
        self.assertEqual(False, connection_base_de_donnee("bbd_auto.sql"))
        self.assertEqual(False, connection_base_de_donnee("base_de_donne.sqlite3"))

    """ Test redondant
    def creation_bdd(self):
        path = "tests/test.sqlite3"
        self.assertEqual(True, creation_bdd(path))
        self.assertEqual(False, creation_bdd(path))
        os.remove(path)
        self.assertEqual(False, creation_bdd(paths))
        self.assertEqual(True, creation_bdd(path))
    """

    def verification_table_bdd(self):
        path = "tests/test.sqlite3"
        table = table_voiture
        self.assertEqual(True, verification_table_bdd(path, table))
        self.assertEqual(False, verification_table_bdd(path))
        self.assertEqual(False, verification_table_bdd(voiture))
        self.assertEqual(False, verification_table_bdd(paths, voture))

    def Insertion_ligne_bdd(self):
        self.assertEqual(True, Insertion_ligne_bdd(row, base_de_donne))
